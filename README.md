# @universis/data-trees

Universis api server Merkle Tree Services for data.

## Installation

npm i @universis/data-trees

## Usage

Register `MerkleTreeStorageService` and `MerkleTreeService` in application services:

    # app.production.json

    "services": [
        ...,
        {
      "serviceType": "@universis/data-trees#MerkleTreeService"
        },
        {
      "serviceType": "@universis/data-trees#MerkleTreeStorageService",
      "strategyType": "@universis/data-trees#MerkleTreeFileStorageService"
        },
    ]

Add `StudentCourseTreeSchemaLoader` to schema loaders

    # app.production.json

    {
        "settings": {
            "schema": {
                "loaders": [
                    ...,
                    {
                        "loaderType": "@universis/data-trees#StudentCourseTreeSchemaLoader"
                    }
                ]
            }
        }
    }

## Configuration

Use `settings/universis/data-trees` section to configure the root of data-trees services:

    "data-trees": {
        "root": "content/private/trees"
      }
