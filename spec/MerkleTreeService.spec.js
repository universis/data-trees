import { MerkleTreeService } from "../src";

describe("MerkleTreeService", () => {
  it("shouldn't verify- wrong schema", async () => {
    const service = new MerkleTreeService();
    const schema = require("./files/student-course-leaf-schema.json");
    const dataPath = "./files/responseWrongSchema.json";
    const rawData = require(dataPath);
    const data = rawData.value.map((item) => {
      return {
        grade: item.grade,
        ects: item.ects,
        coefficient: item.coefficient,
        hours: item.hours,
        units: item.units,
      };
    });
    const leaves = service.createLeaves(data, schema);
    expect(leaves).toBe(null);
  });
  it("should verify", async () => {
    const service = new MerkleTreeService();
    const dataPath = "./files/response.json";
    const rawData = require(dataPath);
    const data = rawData.value.map((item) => {
      return {
        grade: item.grade,
        ects: item.ects,
        coefficient: item.coefficient,
        hours: item.hours,
        units: item.units,
      };
    });

    const merkleTreeContains = {
      tree: {
        options: {
          complete: false,
          isBitcoinTree: false,
          hashLeaves: false,
          sortLeaves: false,
          sortPairs: false,
          sort: false,
          fillDefaultHash: null,
          duplicateOdd: false,
        },
        root: "0x01fc4aa8c95e89a50fba64df2192207a00a4ec9ee78d84c8fccafd462070e8be",
        layers: [
          [
            "0x0b9f5868eb906492a7439428e10cb2ad70dabc932257eec6a4a3b4c4933d864a",
            "0x213125978e929f3a7ae133f7c403366c8b48a8474dca5ffcf3ce77e3b75858ad",
            "0x497dba4730b0f3fe3f7993366cefd7904498af37b52cc9918bfac51de7a2d842",
          ],
          [
            "0x7eac2f4a917a4b66d0c4b68d6741074656b5bf646de297368cdfc4628f2ab719",
            "0x497dba4730b0f3fe3f7993366cefd7904498af37b52cc9918bfac51de7a2d842",
          ],
          [
            "0x01fc4aa8c95e89a50fba64df2192207a00a4ec9ee78d84c8fccafd462070e8be",
          ],
        ],
        leaves: [
          "0x0b9f5868eb906492a7439428e10cb2ad70dabc932257eec6a4a3b4c4933d864a",
          "0x213125978e929f3a7ae133f7c403366c8b48a8474dca5ffcf3ce77e3b75858ad",
          "0x497dba4730b0f3fe3f7993366cefd7904498af37b52cc9918bfac51de7a2d842",
        ],
      },
      hexRoot:
        "0x01fc4aa8c95e89a50fba64df2192207a00a4ec9ee78d84c8fccafd462070e8be",
    };
    const verified = service.verifyMerkleTree(data, merkleTreeContains);
    expect(verified).toBe(true);
  });
  it("should not verify , changed Student Course", async () => {
    const service = new MerkleTreeService();
    const dataPath = "./files/responseFalse.json";
    const rawData = require(dataPath);
    const data = rawData.value.map((item) => {
      return {
        grade: item.grade,
        ects: item.ects,
        coefficient: item.coefficient,
        hours: item.hours,
        units: item.units,
      };
    });
    const merkleTreeContains = {
      tree: {
        options: {
          complete: false,
          isBitcoinTree: false,
          hashLeaves: false,
          sortLeaves: false,
          sortPairs: false,
          sort: false,
          fillDefaultHash: null,
          duplicateOdd: false,
        },
        root: "0x01fc4aa8c95e89a50fba64df2192207a00a4ec9ee78d84c8fccafd462070e8be",
        layers: [
          [
            "0x0b9f5868eb906492a7439428e10cb2ad70dabc932257eec6a4a3b4c4933d864a",
            "0x213125978e929f3a7ae133f7c403366c8b48a8474dca5ffcf3ce77e3b75858ad",
            "0x497dba4730b0f3fe3f7993366cefd7904498af37b52cc9918bfac51de7a2d842",
          ],
          [
            "0x7eac2f4a917a4b66d0c4b68d6741074656b5bf646de297368cdfc4628f2ab719",
            "0x497dba4730b0f3fe3f7993366cefd7904498af37b52cc9918bfac51de7a2d842",
          ],
          [
            "0x01fc4aa8c95e89a50fba64df2192207a00a4ec9ee78d84c8fccafd462070e8be",
          ],
        ],
        leaves: [
          "0x0b9f5868eb906492a7439428e10cb2ad70dabc932257eec6a4a3b4c4933d864a",
          "0x213125978e929f3a7ae133f7c403366c8b48a8474dca5ffcf3ce77e3b75858ad",
          "0x497dba4730b0f3fe3f7993366cefd7904498af37b52cc9918bfac51de7a2d842",
        ],
      },
      hexRoot:
        "0x01fc4aa8c95e89a50fba64df2192207a00a4ec9ee78d84c8fccafd462070e8be",
    };
    const verified = service.verifyMerkleTree(data, merkleTreeContains);
    expect(verified).toBe(false);
  });
});
