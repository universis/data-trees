import { ApplicationService } from "@themost/common";
import { StudentCourseReplacer } from "./StudentCourseReplacer";
import { MerkleTree } from "merkletreejs";
import keccak256 from "keccak256";
import crypto from "crypto";
import Ajv from "ajv";
import { MerkleTreeContains } from "./MerkleTreeStorageService";
const sha256 = (data) => crypto.createHash("sha256").update(data).digest();

export class MerkleTreeService extends ApplicationService {
  constructor(app) {
    super(app);
    // extend universis api scope access configuration
    if (app && app.container) {
      app.container.subscribe((container) => {
        if (container) {
          new StudentCourseReplacer(app).apply();
        }
      });
    }
  }
  /**
   * Creates the leaves of the tree and check if the data are in the correct format
   * @param {any} data The data for the leaves
   * @param {any} schema The correct schema of the items
   */
  createLeaves(data, schema) {
    const ajv = new Ajv(); // options can be passed, e.g. {allErrors: true}
    let wrongSchema = false;
    const validate = ajv.compile(schema);
    // iterate the array of data to validate the schema
    if (data.length > 0) {
      data.forEach((row) => {
        const result = validate(row);

        console.log("Validation Result", result);

        console.log("Validation Errors", validate.errors);
        if (validate.errors != null) {
          wrongSchema = true;
        }
      });
    } else {
      return null;
    }
    if (wrongSchema) {
      //dont return the leaves
      return null;
    }
    // if there are no errors create the hashes with base64 encoding
    const hashes = data.map((item) => {
      return Buffer.from(JSON.stringify(item)).toString("base64");
    });
    //return the leaves with keccak256 encoding
    return hashes.map(keccak256);
  }

  /**
   * Creates a new Merkle Tree
   * @param {String<Array>} leaves The retrieved leaves
   */
  createTree(leaves) {
    const tree = new MerkleTree(leaves, sha256);
    return tree;
  }
  treeToJSON(tree) {
    return JSON.parse(MerkleTree.marshalTree(tree));
  }
  /**
   * Verifies if the Merkle tree is valid
   * @param {Array<String>} courses The courses for verification
   * @param {MerkleTreeContains} savedJson The saved Json of the latest valid Merkle Tree
   */

  verifyMerkleTree(courses, savedJson) {
    const schema = require("./listeners/schema/student-course-leaf-schema.json");
    const tree = MerkleTree.unmarshalTree(savedJson.tree, sha256);
    const leaves = this.createLeaves(courses, schema);
    // iterate and verify each leaf of the tree
    for (let i = 0; i < leaves.length; i++) {
      const proof = tree.getProof(leaves[i]);
      const verified = tree.verify(proof, leaves[i], savedJson.hexRoot, sha256);
      // if it is not verified return false
      if (!verified) {
        return false;
      }
    }
    return true;
  }
}
