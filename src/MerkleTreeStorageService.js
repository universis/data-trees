import { AbstractMethodError, ApplicationService } from "@themost/common";
import { promisify } from "es6-promisify";

class MerkleTreeStorageService extends ApplicationService {
  constructor(app) {
    super(app);
  }
  /**
   * @abstract
   * @param {*} key
   */
  read(key) {
    throw new AbstractMethodError();
  }
  /**
   * @abstract
   * @param {*} key
   * @param {*} data
   */
  write(key, data) {
    throw new AbstractMethodError();
  }
  /**
   * @abstract
   * @param {*} key
   */
  exists(key) {
    throw new AbstractMethodError();
  }
}

class MerkleTreeFileStorageService extends MerkleTreeStorageService {
  /**
   *
   * @param {import("@themost/express").ExpressDataApplication} app
   */
  constructor(app) {
    super(app);
    this.contentRoot = app
      .getConfiguration()
      .getSourceAt("settings/universis/data-trees/root");
  }
  async read(key) {
    const path = require("node:path");
    const fs = require("fs");
    // make fs functions to promises
    const mkdir = promisify(fs.mkdir);
    const readFile = promisify(fs.readFile);
    //take the filename
    const filename = path.resolve(
      this.contentRoot,
      key.model,
      require("@themost/common").Base26Number.toBase26(key.key) + ".json"
    );
    try {
      const data = await readFile(filename, "utf8");
      try {
        //make it JSON object
        const jsonObject = JSON.parse(data);
        return jsonObject;
      } catch (parseError) {
        console.error("Error parsing JSON:", parseError);
      }
    } catch (err) {
      console.error("Something wrong!", err);
    }
  }
  async write(key, data) {
    const path = require("node:path");
    const fs = require("fs");
    // make fs functions to promises
    const mkdir = promisify(fs.mkdir);
    const writeFile = promisify(fs.writeFile);
    //take the filename
    const filename = path.resolve(
      this.contentRoot,
      key.model,
      require("@themost/common").Base26Number.toBase26(key.key) + ".json"
    );

    const directory = path.resolve(this.contentRoot, key.model);

    try {
      // Create directories if they don't exist
      await mkdir(directory, { recursive: true });
      // Write the file
      await writeFile(filename, data);
      console.log("File created successfully:", filename);
    } catch (err) {
      console.error("Something went wrong:", err);
    }
  }
  async exists(key) {
    const path = require("node:path");
    const fs = require("fs");
    // make fs functions to promises
    const access = promisify(fs.access);
    //take the filename
    const filename = path.resolve(
      this.contentRoot,
      key.model,
      require("@themost/common").Base26Number.toBase26(key.key) + ".json"
    );
    try {
      await access(filename);
      console.log("File exists.");
      return true;
    } catch (error) {
      console.error("File does not exist.");
      return false;
    }
  }
}
export { MerkleTreeFileStorageService, MerkleTreeStorageService };
