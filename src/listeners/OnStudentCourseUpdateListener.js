import { DataError } from "@themost/common";
import { MerkleTreeService } from "../MerkleTreeService";
import { DataObjectState } from "@themost/data";
import { MerkleTreeStorageService } from "../MerkleTreeStorageService";
// eslint-disable-next-line no-unused-vars
async function beforeSaveAsync(event) {
  const context = event.model.context;
  //get the services
  const service = context.application.getService(MerkleTreeService);
  const storageService = context.application.getService(
    MerkleTreeStorageService
  );
  //take the correct schema and collect the properties
  const schema = require("./schema/student-course-leaf-schema.json");
  const selectProperties = Object.keys(schema.properties).map((key) => key);
  //get the courses of the target student and specify the properties we want
  const courses = await context
    .model("StudentCourse")
    .where("student")
    .equal(event.target.student)
    .and("isPassed")
    .equal(true)
    .select(selectProperties)
    .orderBy("dateModified")
    .getItems();

  if (courses.length > 0) {
    const merkleTreeKey = {
      model: "StudentCourseTrees",
      key: event.target.student,
    };
    //check the existance of the file of the student
    if (await storageService.exists(merkleTreeKey)) {
      // if it exists read the data and verify the courses
      const savedJson = await storageService.read(merkleTreeKey);
      const verified = service.verifyMerkleTree(courses, savedJson);
      if (!verified) {
        throw new DataError(
          "ERR_VERIFIED",
          "Unverified data",
          JSON.stringify(courses),
          null,
          "StudentCourse",
          null
        );
      }
    } else {
      throw new DataError(
        "ERR_TREE_NOENT",
        "File does not exist",
        null,
        "StudentCourse",
        null
      );
    }
  }
}
async function beforeRemoveAsync(event) {
  const context = event.model.context;
  //get the services
  const service = context.application.getService(MerkleTreeService);
  const storageService = context.application.getService(
    MerkleTreeStorageService
  );
  //take the correct schema and collect the properties
  const schema = require("./schema/student-course-leaf-schema.json");
  const selectProperties = Object.keys(schema.properties).map((key) => key);
  //get the courses of the target student and specify the properties we want
  const courses = await context
    .model("StudentCourse")
    .where("student")
    .equal(event.previous.student)
    .and("isPassed")
    .equal(true)
    .select(selectProperties)
    .orderBy("dateModified")
    .getItems();

  const merkleTreeKey = {
    model: "StudentCourseTrees",
    key: event.previous.student,
  };
  //check the existance of the file of the student
  if (await storageService.exists(merkleTreeKey)) {
    // if it exists read the data and verify the courses
    const savedJson = await storageService.read(merkleTreeKey);
    const verified = service.verifyMerkleTree(courses, savedJson);
    if (!verified) {
      throw new DataError(
        "ERR_VERIFIED",
        "Unverified data",
        null,
        "StudentCourse",
        null
      );
    }
  } else {
    throw new DataError(
      "ERR_TREE_NOENT",
      "File does not exist",
      null,
      "StudentCourse",
      null
    );
  }
}
/**
 *
 * @param {import('@themost/data').DataEventArgs} event
 */
async function afterRemoveAsync(event) {
  // get context
  // eslint-disable-next-line no-unused-vars
  /**
   *
   * @type {import('@themost/express').ExpressDataContext}
   */
  const context = event.model.context;
  //get the services
  const service = context.application.getService(MerkleTreeService);
  const storageService = context.application.getService(
    MerkleTreeStorageService
  );
  const schema = require("./schema/student-course-leaf-schema.json");
  const selectProperties = Object.keys(schema.properties).map((key) => key);
  //get the courses of the target student and specify the properties we want
  const courses = await context
    .model("StudentCourse")
    .where("student")
    .equal(event.previous.student)
    .and("isPassed")
    .equal(true)
    .select(selectProperties)
    .orderBy("dateModified")
    .getItems();
  // create the leaves and the tree
  const leaves = service.createLeaves(courses, schema);
  const tree = service.createTree(leaves);
  const merkleTreeKey = {
    model: "StudentCourseTrees",
    key: event.previous.student,
  };
  const merkleTreeContains = {
    tree: service.treeToJSON(tree),
    hexRoot: tree.getHexRoot(),
  };
  //make the JSON object to string and write it to the file of student
  const jsonString = JSON.stringify(merkleTreeContains, null, 2);
  await storageService.write(merkleTreeKey, jsonString);
  console.log("Succesfully Operation");
}

/**
 *
 * @param {import('@themost/data').DataEventArgs} event
 */
async function afterSaveAsync(event) {
  // get context
  // eslint-disable-next-line no-unused-vars
  /**
   *
   * @type {import('@themost/express').ExpressDataContext}
   */
  const context = event.model.context;
  //get the services
  const service = context.application.getService(MerkleTreeService);
  const storageService = context.application.getService(
    MerkleTreeStorageService
  );

  const schema = require("./schema/student-course-leaf-schema.json");
  const selectProperties = Object.keys(schema.properties).map((key) => key);
  //continue if one of the selected properties are modified from the call, or it is an insert call
  let checkMerkle = false;
  if (event.state == DataObjectState.Update) {
    selectProperties.forEach((property) => {
      if (Object.prototype.hasOwnProperty.call(event.target, property)) {
        checkMerkle = true;
      }
    });
  }
  if (event.state === DataObjectState.Insert || checkMerkle) {
    //get the courses of the target student and specify the properties we want
    const courses = await context
      .model("StudentCourse")
      .where("student")
      .equal(event.target.student)
      .and("isPassed")
      .equal(true)
      .select(selectProperties)
      .orderBy("dateModified")
      .getItems();
    // create the leaves and the tree
    const leaves = service.createLeaves(courses, schema);
    const tree = service.createTree(leaves);
    const merkleTreeKey = {
      model: "StudentCourseTrees",
      key: event.target.student,
    };
    const merkleTreeContains = {
      tree: service.treeToJSON(tree),
      hexRoot: tree.getHexRoot(),
    };
    //make the JSON object to string and write it to the file of student
    const jsonString = JSON.stringify(merkleTreeContains, null, 2);
    await storageService.write(merkleTreeKey, jsonString);
    console.log("Succesfully Operation");
  }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
  return beforeSaveAsync(event)
    .then(() => {
      return callback();
    })
    .catch((err) => {
      return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
  return afterSaveAsync(event)
    .then(() => {
      return callback();
    })
    .catch((err) => {
      return callback(err);
    });
}

export function beforeRemove(event, callback) {
  return beforeRemoveAsync(event)
    .then(() => {
      return callback();
    })
    .catch((err) => {
      return callback(err);
    });
}
export function afterRemove(event, callback) {
  return afterRemoveAsync(event)
    .then(() => {
      return callback();
    })
    .catch((err) => {
      return callback(err);
    });
}
