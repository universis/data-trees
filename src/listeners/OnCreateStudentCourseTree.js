import { DataError } from "@themost/common";
import { MerkleTreeService } from "../MerkleTreeService";
import { DataObjectState } from "@themost/data";
import { MerkleTreeStorageService } from "../MerkleTreeStorageService";
// eslint-disable-next-line no-unused-vars
async function beforeSaveAsync(event) {
  //
}

async function afterSaveAsync(event) {
  // get context
  // eslint-disable-next-line no-unused-vars
  const context = event.model.context;
  //get the services
  const service = context.application.getService(MerkleTreeService);
  const storageService = context.application.getService(
    MerkleTreeStorageService
  );
  //take the correct schema and collect the properties
  const schema = require("./schema/student-course-leaf-schema.json");
  const selectProperties = Object.keys(schema.properties).map((key) => key);
  // get current status
  const actionStatus = await event.model
    .where("id")
    .equal(event.target.id)
    .select("actionStatus/alternateName")
    .value();
  // get previous status
  let previousStatus = "UnknownActionStatus";
  if (event.previous) {
    previousStatus = event.previous.actionStatus.alternateName;
  }
  if (
    (actionStatus === "ActiveActionStatus" &&
      event.state === DataObjectState.Insert) ||
    (actionStatus === "ActiveActionStatus" &&
      previousStatus === "PotentialActionStatus" &&
      event.state === DataObjectState.Update)
  ) {
    //get the courses of the target student and specify the properties we want
    const courses = await context
      .model("StudentCourse")
      .where("student")
      .equal(event.target.student)
      .and("isPassed")
      .equal(true)
      .select(selectProperties)
      .orderBy("dateModified")
      .getItems();

    if (courses.length > 0) {
      const merkleTreeKey = {
        model: "StudentCourseTrees",
        key: event.target.student,
      };

      //check the existance of the file of the student
      if (await storageService.exists(merkleTreeKey)) {
        // if it exists read the data and verify the courses
        const savedJson = await storageService.read(merkleTreeKey);
        const verified = service.verifyMerkleTree(courses, savedJson);
        if (!verified) {
          throw new DataError(
            "ERR_VERIFIED",
            "Unverified data",
            JSON.stringify(courses),
            null,
            "StudentCourse",
            null
          );
        }
      } else {
        // if file does not exist, create it and write the json string
        // create the leaves and the tree
        const leaves = service.createLeaves(courses, schema);
        const tree = service.createTree(leaves);
        const merkleTreeContains = {
          tree: service.treeToJSON(tree),
          hexRoot: tree.getHexRoot(),
        };
        //json object to string
        const jsonString = JSON.stringify(merkleTreeContains, null, 2);
        await storageService.write(merkleTreeKey, jsonString);
      }
    }
  }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
  return beforeSaveAsync(event)
    .then(() => {
      return callback();
    })
    .catch((err) => {
      return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
  return afterSaveAsync(event)
    .then(() => {
      return callback();
    })
    .catch((err) => {
      return callback(err);
    });
}
