import { ApplicationService } from "@themost/common";
import MerkleTree from "merkletreejs";
export interface MerkleTreeKey {
  model: string;
  key: string;
}
export interface MerkleTreeContains {
  tree: any;
  hexRoot: string;
}
export declare abstract class MerkleTreeStorageService extends ApplicationService {
  abstract read(key: MerkleTreeKey): Promise<MerkleTreeContains>;
  abstract exists(key: MerkleTreeKey): Promise<boolean>;
  abstract write(key: MerkleTreeKey, data: any): Promise<void>;
}
export declare class MerkleTreeFileStorageService extends MerkleTreeStorageService {
  read(key: MerkleTreeKey): Promise<MerkleTreeContains>;
  exists(key: MerkleTreeKey): Promise<boolean>;
  write(key: MerkleTreeKey, data: any): Promise<void>;
}
