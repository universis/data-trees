import { ApplicationService } from "@themost/common";
import { SchemaLoaderStrategy } from "@themost/data";
import path from "path";

export class StudentCourseReplacer extends ApplicationService {
  constructor(app) {
    super(app);
  }

  apply() {
    // get schema loader
    const schemaLoader = this.getApplication()
      .getConfiguration()
      .getStrategy(SchemaLoaderStrategy);
    // get model definition
    const model = schemaLoader.getModelDefinition("StudentCourse");
    model.eventListeners = model.eventListeners || [];
    model.eventListeners.push({
      type: path.resolve(__dirname, "listeners/OnStudentCourseUpdateListener"),
    });
    model.versioning = true;
    schemaLoader.setModelDefinition(model);
  }
}
