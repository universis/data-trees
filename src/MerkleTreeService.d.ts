import { ApplicationService } from "@themost/common";
import { MerkleTree } from "merkletreejs";
import { MerkleTreeContains } from "./MerkleTreeStorageService";

export declare class MerkleTreeService extends ApplicationService {
  createLeaves(data: any, schema: any): Array<String> | null;
  createTree(leaves: Array<String>): MerkleTree;
  treeToJSON(tree: MerkleTree): string;
  verifyMerkleTree(
    courses: Array<String>,
    savedJson: MerkleTreeContains
  ): Boolean;
}
